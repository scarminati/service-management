﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_Management
{
    class Cliente
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string PIva { get; set; }
        public string CodiceFiscale { get; set; }
        public string Indirizzo { get; set; }
        public string Provincia { get; set; }
        public string Paese { get; set; }
        public string CAP { get; set; }
        public string Stato { get; set; }
        public string Telefono { get; set; }
        public string SitoInternet { get; set; }
        public string Fax { get; set; }
        public string EMail { get; set; }

        public Cliente()
        {

        }

        public Cliente(string nome, string pIva, string codiceFiscale, string indirizzo, string provincia, string paese, string cap, string stato, string telefono, string sitoInternet, string fax, string eMail)
        {
            this.Nome = nome;
            this.PIva = pIva;
            this.CodiceFiscale = codiceFiscale;
            this.Indirizzo = indirizzo;
            this.Provincia = provincia;
            this.Paese = paese;
            this.CAP = cap;
            this.Stato = stato;
            this.Telefono = telefono;
            this.SitoInternet = sitoInternet;
            this.Fax = fax;
            this.EMail = eMail;
        }

    }
}
