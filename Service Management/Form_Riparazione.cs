﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Service_Management
{
    public partial class Form_Riparazione : Form
    {
        private int indexRecordToEdit = -1;
        private Dictionary<String, int> extKeys = new Dictionary<String, int> { { "Id_Cliente", -1 },{ "Id_Articolo", -1 } };
        private DataTable dt_Rip;
        private DataTable dt_Cli;
        private DataTable dt_Art;
        public Form_Riparazione()
        {
            try
            {
                InitializeComponent();
                setDateTimePicker();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Riparazione(int indexRecordToEdit) : this()
        {
            this.indexRecordToEdit = indexRecordToEdit;
            fillForm();
        }

        private void setDateTimePicker()
        {
            dtp_dataOrdRip.Value = DateTime.Now;
            dtp_dataDocReso.Value = DateTime.Now;
            dtp_DataRiparazione.Value = DateTime.Now;
        }

        private void fillForm()
        {
            try
            {
                dt_Rip = Utility.getDataTableFromDB("SELECT * FROM [Riparazioni] WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                if (dt_Rip.Rows.Count.Equals(1))
                {
                    dt_Cli = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE [Id] = '" + dt_Rip.Rows[0].Field<int>("Id Cliente").ToString() + "'");
                    dt_Art = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE [Id] = '" + dt_Rip.Rows[0].Field<int>("Id Articolo").ToString() + "'");
                    tb_NomeCliente.Text = dt_Cli.Rows[0].Field<string>("Nome");
                    tb_scarCliente.Text = dt_Rip.Rows[0].Field<string>("SCAR Cliente");
                    tb_numScarCLI.Text = dt_Rip.Rows[0].Field<string>("Numero SCAR Cliente");
                    tb_ordRiparaz.Text = dt_Rip.Rows[0].Field<string>("Ordine Di Riparazione");
                    dtp_dataOrdRip.Value = dt_Rip.Rows[0].Field<DateTime>("Data Ordine Di Riparazione");
                    tb_docReso.Text = dt_Rip.Rows[0].Field<string>("Documento Di Reso");
                    dtp_dataDocReso.Value = dt_Rip.Rows[0].Field<DateTime>("Data Documento Di Reso");
                    tb_difettoSegnalatoDalCliente.Text = dt_Rip.Rows[0].Field<string>("Difetto Segnalato Dal Cliente");
                    tb_Articolo.Text = dt_Art.Rows[0].Field<string>("Descrizione");
                    tb_CodiceArticoloCliente.Text = dt_Rip.Rows[0].Field<string>("Codice Articolo Cliente");
                    tb_VersioneArticolo.Text = dt_Rip.Rows[0].Field<string>("Versione Articolo");
                    tb_SerialeArticolo.Text = dt_Rip.Rows[0].Field<string>("Seriale Articolo");
                    tb_LottoArticolo.Text = dt_Rip.Rows[0].Field<string>("Lotto Articolo");
                    tb_NoteArticolo.Text = dt_Rip.Rows[0].Field<string>("Note Articolo");
                    cmb_CausaDifetto.SelectedItem = dt_Rip.Rows[0].Field<string>("Causa Difetto");
                    tb_difettoRiscontrato.Text = dt_Rip.Rows[0].Field<string>("Difetto Riscontrato");
                    cmb_tipoRiparazione.SelectedItem = dt_Rip.Rows[0].Field<string>("Tipo Riparazione");
                    tb_dettaglioRiparazione.Text = dt_Rip.Rows[0].Field<string>("Dettaglio Riparazione");
                    tb_costoPreventivato.Text = dt_Rip.Rows[0].Field<decimal>("Costo Preventivato").ToString().PadLeft(8, '0');
                    tb_costoSostenuto.Text = dt_Rip.Rows[0].Field<decimal>("Costo Sostenuto").ToString().PadLeft(8, '0');
                    tb_costoAddebitato.Text = dt_Rip.Rows[0].Field<decimal>("Costo Addebitato").ToString().PadLeft(8, '0');
                    cmb_statoRiparazione.SelectedItem = dt_Rip.Rows[0].Field<string>("Stato Riparazione");
                    dtp_DataRiparazione.Value = dt_Rip.Rows[0].Field<DateTime>("Data Riparazione");
                    tb_noteRiparazione.Text = dt_Rip.Rows[0].Field<string>("Note Riparazione");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Cliente
        private void tb_Nome_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_NomeCliente.Text.Equals(""))
                    tb_NomeCliente.BackColor = Color.Salmon;
                else
                    tb_NomeCliente.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cercaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = Utility.getRecordFromDBTable("Clienti");
                if (!dt.Rows.Count.Equals(0))
                {
                    extKeys["Id_Cliente"] = dt.Rows[0].Field<int>("Id");
                    tb_NomeCliente.Text = dt.Rows[0].Field<String>("Nome");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Articolo
        private void tb_Articolo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tb_Articolo.Text.Equals(""))
                    tb_Articolo.BackColor = Color.Salmon;
                else
                    tb_Articolo.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cercaArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = Utility.getRecordFromDBTable("Articoli");
                if (!dt.Rows.Count.Equals(0))
                {
                    extKeys["Id_Articolo"] = dt.Rows[0].Field<int>("Id");
                    tb_Articolo.Text = dt.Rows[0].Field<String>("Descrizione");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_NomeCliente.Text.Equals("") || tb_Articolo.Text.Equals(""))
                    MessageBox.Show("Completare i campi obbligatori prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (indexRecordToEdit.Equals(-1))
                        searchRecordInDB();
                    else
                        updateRecordInDB();
                    if (this.DialogResult.Equals(DialogResult.OK))
                        this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                dt_Cli = Utility.getDataTableFromDB("SELECT * FROM [Clienti] WHERE [Nome] = '" + tb_NomeCliente.Text + "'");
                dt_Art = Utility.getDataTableFromDB("SELECT * FROM [Articoli] WHERE [Descrizione] = '" + tb_Articolo.Text + "'");
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Riparazioni] WHERE " +
                       //"[Data_Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") +"'");
                       "[Id Cliente] = '" + dt_Cli.Rows[0].Field<int>("Id") + "' AND " +
                       "[SCAR Cliente] = '" + tb_scarCliente.Text + "' AND " +
                       "[Numero SCAR Cliente] = '" + tb_numScarCLI.Text + "' AND " +
                       "[Ordine Di Riparazione] = '" + tb_ordRiparaz.Text + "' AND " +
                       "[Data Ordine Di Riparazione] = '" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Documento Di Reso] = '" + tb_docReso.Text + "' AND " +
                       "[Data Documento Di Reso] = '" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Difetto Segnalato Dal Cliente] = '" + tb_difettoSegnalatoDalCliente.Text + "' AND " +
                       "[Id Articolo] = '" + dt_Art.Rows[0].Field<int>("Id") + "' AND " +
                       "[Codice Articolo Cliente] = '" + tb_CodiceArticoloCliente.Text + "' AND " +
                       "[Versione Articolo] = '" + tb_VersioneArticolo.Text + "' AND " +
                       "[Seriale Articolo] = '" + tb_SerialeArticolo.Text + "' AND " +
                       "[Lotto Articolo] = '" + tb_LottoArticolo.Text + "' AND " +
                       "[Note Articolo] = '" + tb_NoteArticolo.Text + "' AND " +
                       "[Causa Difetto] = '" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "' AND " +
                       "[Difetto Riscontrato] = '" + tb_difettoRiscontrato.Text + "' AND " +
                       "[Tipo Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "' AND " +
                       "[Dettaglio Riparazione] = '" + tb_dettaglioRiparazione.Text + "' AND " +
                       "[Costo Preventivato] = '" + tb_costoPreventivato.Text.Replace(' ', '0') + "' AND " +
                       "[Costo Sostenuto] = '" + tb_costoSostenuto.Text.Replace(' ', '0') + "' AND " +
                       "[Costo Addebitato] = '" + tb_costoAddebitato.Text.Replace(' ', '0') + "' AND " +
                       "[Stato Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "' AND " +
                       "[Data Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' AND " +
                       "[Note Riparazione] = '" + tb_noteRiparazione.Text + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                Utility.executeQuery("INSERT INTO [Riparazioni] " +
                "([Id Cliente],[SCAR Cliente],[Numero SCAR Cliente],[Ordine Di Riparazione],[Data Ordine Di Riparazione],[Documento Di Reso],[Data Documento Di Reso],[Difetto Segnalato Dal Cliente],[Id Articolo],[Codice Articolo Cliente],[Versione Articolo],[Seriale Articolo],[Lotto Articolo],[Note Articolo],[Causa Difetto],[Difetto Riscontrato],[Tipo Riparazione],[Dettaglio Riparazione],[Costo Preventivato],[Costo Sostenuto],[Costo Addebitato],[Stato Riparazione],[Data Riparazione],[Note Riparazione]) " +
                "VALUES ('" + dt_Cli.Rows[0].Field<int>("Id") + "'," +
                    "'" + tb_scarCliente.Text + "'," +
                    "'" + tb_numScarCLI.Text + "'," +
                    "'" + tb_ordRiparaz.Text + "'," +
                    "'" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_docReso.Text + "'," +
                    "'" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_difettoSegnalatoDalCliente.Text + "'," +
                    "'" + dt_Art.Rows[0].Field<int>("Id") + "'," +
                    "'" + tb_CodiceArticoloCliente.Text + "'," +
                    "'" + tb_VersioneArticolo.Text + "'," +
                    "'" + tb_SerialeArticolo.Text + "'," +
                    "'" + tb_LottoArticolo.Text + "'," +
                    "'" + tb_NoteArticolo.Text + "'," +
                    "'" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "'," +
                    "'" + tb_difettoRiscontrato.Text + "'," +
                    "'" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                    "'" + tb_dettaglioRiparazione.Text + "'," +
                    "'" + tb_costoPreventivato.Text.Replace(' ', '0') + "'," +
                    "'" + tb_costoSostenuto.Text.Replace(' ', '0') + "'," +
                    "'" + tb_costoAddebitato.Text.Replace(' ', '0') + "'," +
                    "'" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                    "'" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                    "'" + tb_noteRiparazione.Text + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [Riparazioni] SET " +
                "[Id Cliente] = '" + dt_Cli.Rows[0].Field<int>("Id") + "'," +
                "[SCAR Cliente] = '" + tb_scarCliente.Text + "'," +
                "[Numero SCAR Cliente] = '" + tb_numScarCLI.Text + "'," +
                "[Ordine Di Riparazione] = '" + tb_ordRiparaz.Text + "'," +
                "[Data Ordine Di Riparazione] = '" + dtp_dataOrdRip.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Documento Di Reso] = '" + tb_docReso.Text + "'," +
                "[Data Documento Di Reso] = '" + dtp_dataDocReso.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Difetto Segnalato Dal Cliente] = '" + tb_difettoSegnalatoDalCliente.Text + "'," +
                "[Id Articolo] = '" + dt_Art.Rows[0].Field<int>("Id") + "'," +
                "[Codice Articolo Cliente] = '" + tb_CodiceArticoloCliente.Text + "'," +
                "[Versione Articolo] = '" + tb_VersioneArticolo.Text + "'," +
                "[Seriale Articolo] = '" + tb_SerialeArticolo.Text + "'," +
                "[Lotto Articolo] = '" + tb_LottoArticolo.Text + "'," +
                "[Note Articolo] = '" + tb_NoteArticolo.Text + "'," +
                "[Causa Difetto] = '" + (cmb_CausaDifetto.SelectedItem != null ? cmb_CausaDifetto.SelectedItem.ToString() : "") + "'," +
                "[Difetto Riscontrato] = '" + tb_difettoRiscontrato.Text + "'," +
                "[Tipo Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                "[Dettaglio Riparazione] = '" + tb_dettaglioRiparazione.Text + "'," +
                "[Costo Preventivato] = '" + tb_costoPreventivato.Text.Replace(' ', '0') + "'," +
                "[Costo Sostenuto] = '" + tb_costoSostenuto.Text.Replace(' ', '0') + "'," +
                "[Costo Addebitato] = '" + tb_costoAddebitato.Text.Replace(' ', '0') + "'," +
                "[Stato Riparazione] = '" + (cmb_tipoRiparazione.SelectedItem != null ? cmb_tipoRiparazione.SelectedItem.ToString() : "") + "'," +
                "[Data Riparazione] = '" + dtp_DataRiparazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Note Riparazione] = '" + tb_noteRiparazione.Text + "' " +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
