﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Drawing;
using System.IO;
using Xceed.Words.NET;
using System.Data.Sql;

namespace Service_Management
{
    public static class Utility
    {
        /// <summary>
        /// esegue una query sul DB e restituisce i dati sotto forma di DataTable
        /// </summary>
        /// <param name="sqlCommand">query da eseguire</param>
        /// <returns></returns>
        public static DataTable getDataTableFromDB(string sqlCommand)
        {
            try
            {
                //STRINGA CO DB OK
                string connectionString = ConfigurationManager.ConnectionStrings["PATH_MySQLDB"].ConnectionString + " AttachDBFilename = " + Environment.CurrentDirectory + @"\Database\ServiceManagementDB.mdf";
                //string connectionString = @"data source =(localdb)\v11.0; Integrated Security = true; AttachDBFilename = " + Environment.CurrentDirectory + @"\Database\ServiceManagementDB.mdf";

                //string connectionString = @"Data Source=(localdb)\v11.0;Initial Catalog=ServiceManagementDB;Integrated Security=True";
                //string connectionString = @"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = Database_ServiceManagement; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = True; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";
                //string connectionString = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\Personal\ServiceManagement\Service Management\Database\Database1.mdf; Initial Catalog = Database1; Integrated Security = True";
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlDataAdapter adapt = new SqlDataAdapter(sqlCommand, connection);
                connection.Close();
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                return dt;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public static void createDatabase()
        {
            try
            {
                string createDBScript = File.ReadAllText(Environment.CurrentDirectory + @"\Database\createDB.txt");
                createDBScript = createDBScript.Replace("@@@PATH@@@", Environment.CurrentDirectory + @"\Database");
                executeQuery(createDBScript);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static int executeQuery(string sqlCommand)
        {
            try
            {
                int retValue = 0;
                //STRINGA CO DB OK
                string connectionString = @"data source =(localdb)\v11.0; Integrated Security = true; AttachDBFilename = " + Environment.CurrentDirectory + @"\Database\ServiceManagementDB.mdf";
                
                
                //string connectionString = @"Data Source=(localdb)\v11.0;Initial Catalog=ServiceManagementDB;Integrated Security=True";
                //string connectionString = @"Data Source=(LocalDB)\mssqllocaldb; AttachDbFilename=" + Environment.CurrentDirectory + ConfigurationManager.AppSettings["PATH_MySQLDB"] + ";Integrated Security=True";
                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                retValue = command.ExecuteNonQuery();
                connection.Close();
                return (retValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return -1;
            }
        }

        /// <summary>
        /// Esegue una query su tutti i campi della tabella
        /// </summary>
        /// <param name="tableName">tabella su cui eseguire la query</param>
        /// <param name="dataToSearch">dato da cercare in ogni record della tabella</param>
        /// <returns></returns>
        public static string getSQLCondition_ForeachColumn(string tableName, string dataToSearch)
        {
            try
            {
                string selectCondition = "";
                DataTable dt = getDataTableFromDB("SELECT TOP(1) * FROM [" + tableName + "]");
                foreach (DataColumn col in dt.Columns)
                {
                    if (selectCondition.Equals(""))
                        selectCondition = " WHERE " + col + " LIKE '%" + dataToSearch + "%'";
                    else
                        selectCondition = selectCondition + " OR " + col + " LIKE '%" + dataToSearch + "%'";
                }
                return selectCondition;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DataTable aggiornaDataGridView(string sqlCommand)
        {
            try
            {
                return(Utility.getDataTableFromDB(sqlCommand));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        /// <summary>
        /// Apre il form di ricerca e restituisce la DataTable con i risultati
        /// </summary>
        /// <param name="tableName">Nome della tabella in cui cercare il record</param>
        /// <returns></returns>
        public static DataTable getRecordFromDBTable(String tableName)
        {
            DataTable dt = new DataTable();
            Form_RicercaDatabase frdb = new Form_RicercaDatabase(tableName);
            var dialogResult = frdb.ShowDialog();
            if (dialogResult.Equals(DialogResult.OK))
            {
                dt = Utility.aggiornaDataGridView("SELECT * FROM [" + tableName + "] WHERE Id = '" + frdb.indexRecordToEdit + "'");
                frdb.Dispose();
            }
            return (dt);
        }

        public static void saveAndVisualizeExcelReport(DataGridView dgv, string workSheetName)
        {
            try
            {
                ExcelPackage excel = new ExcelPackage();
                //Add the worksheet Report
                excel.Workbook.Worksheets.Add(workSheetName);
                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(dgv.ColumnCount + 64) + "1";
                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets[workSheetName];
                // Popular header row data
                DataTable dataTable = (DataTable)(dgv.DataSource);
                worksheet.Cells[headerRange].LoadFromDataTable(dataTable, true);
                //set cells layout
                worksheet.Cells[1, 1, 1, dgv.ColumnCount].Style.Font.Bold = true;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[1, 1, dgv.RowCount, dgv.ColumnCount].AutoFitColumns();
                worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                worksheet.PrinterSettings.FitToPage = true;
                //save the excel file report
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\");
                FileInfo excelFile = new FileInfo(Environment.CurrentDirectory + @"\Reports\Extract_" + workSheetName + ".xlsx");
                excel.SaveAs(excelFile);
                System.Diagnostics.Process.Start(Environment.CurrentDirectory + @"\Reports\Extract_" + workSheetName + ".xlsx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void saveAndVisualizeWordReport(DataGridView dgv, int index)
        {
            try
            {
                string pathTemplate = Environment.CurrentDirectory + @"\Templates\Report_Template.docx";
                var doc = DocX.Load(pathTemplate);
                //salvataggio del file
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Reports\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Reports\");
                string pathReport = Environment.CurrentDirectory + @"\Reports\Report_" + DateTime.Now.ToString().Replace('/','_').Replace(':', '_').Replace(' ', 'h') + ".docx";
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [Settings]");
                doc.ReplaceText("@@RS_DSC@@", dt.Rows[0].Field<string>("Ragione Sociale"));
                dt = Utility.getDataTableFromDB("SELECT * FROM [Vista_Riparazioni]");
                if (!dt.Rows.Count.Equals(0))
                {
                    doc.ReplaceText("@@NSM@@", dt.Rows[0].Field<int>("ID").ToString());
                    doc.ReplaceText("@@CLI_NOME@@", dt.Rows[0].Field<string>("Nome").ToString());
                    doc.ReplaceText("@@CLI_PIVA@@", dt.Rows[0].Field<string>("P. IVA").ToString());
                    doc.ReplaceText("@@CLI_ADDR@@", dt.Rows[0].Field<string>("Indirizzo").ToString());
                    doc.ReplaceText("@@CLI_PAESE@@", dt.Rows[0].Field<string>("Paese").ToString());
                    doc.ReplaceText("@@CAP@@", dt.Rows[0].Field<string>("CAP").ToString());
                    doc.ReplaceText("@@PROV@@", dt.Rows[0].Field<string>("Provincia").ToString());
                    doc.ReplaceText("@@STATO@@", dt.Rows[0].Field<string>("Stato").ToString());

                    doc.ReplaceText("@@ART_NOME@@", dt.Rows[0].Field<string>("Descrizione").ToString());
                    doc.ReplaceText("@@ART_VERS@@", dt.Rows[0].Field<string>("Versione Articolo").ToString());
                    doc.ReplaceText("@@ART_SER@@", dt.Rows[0].Field<string>("Seriale Articolo").ToString());
                    doc.ReplaceText("@@ART_LOT@@", dt.Rows[0].Field<string>("Lotto Articolo").ToString());

                    doc.ReplaceText("@@NC_SCAR@@", dt.Rows[0].Field<string>("SCAR Cliente").ToString());
                    doc.ReplaceText("@@NC_NSCAR@@", dt.Rows[0].Field<string>("Numero SCAR Cliente").ToString());
                    doc.ReplaceText("@@NC_ORDR@@", dt.Rows[0].Field<string>("Ordine Di Riparazione").ToString());
                    doc.ReplaceText("@@NC_DTORDR@@", dt.Rows[0].Field<DateTime>("Data Ordine Di Riparazione").ToShortDateString());
                    doc.ReplaceText("@@NC_DIF@@", dt.Rows[0].Field<string>("Difetto Segnalato Dal Cliente").ToString());
                    doc.ReplaceText("@@NC_NOTE@@", dt.Rows[0].Field<string>("Lotto Articolo").ToString());

                    doc.ReplaceText("@@RP_TIPO@@", dt.Rows[0].Field<string>("Tipo Riparazione").ToString());
                    doc.ReplaceText("@@RP_DETRIP@@", dt.Rows[0].Field<string>("Dettaglio Riparazione").ToString());
                    doc.ReplaceText("@@RP_NOTERIP@@", dt.Rows[0].Field<string>("Note Riparazione").ToString());
                    doc.ReplaceText("@@RP_DTRIP@@", dt.Rows[0].Field<DateTime>("Data Riparazione").ToShortDateString());
                    doc.ReplaceText("@@RP_CRIP@@", dt.Rows[0].Field<decimal>("Costo Addebitato").ToString());

                    doc.SaveAs(pathReport);
                    System.Diagnostics.Process.Start(pathReport);
                }
                else
                    MessageBox.Show("Impossible salvare il report.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
